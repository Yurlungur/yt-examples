#!/usr/bin/env python
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2015-08-13 23:18:27 (jmiller)>

# This is a simple script using the (very preliminary!) yt frontend
# for Carpet. The frontend is in the bitbucket repository (a fork of
# yt) for one of the yt developers, Kacper Krowalik. yt uses Mercurial
# and the appropriate version is the "carpet" bookmark.
# https://bitbucket.org/xarthisius/yt/src/84e1c765c8355a9606d92687c10445bec37de9f6/?at=cactus
# To get it:
# hg clone ssh://hg@bitbucket.org/xarthisius/yt
# cd yt
# hg bookmark cactus
# python setup.py build_ext -i
# if this works, you installed yt. Now just add $(pwd) to your PYTHONPATH

# WARNING. The Carpet frontend is a work in progress and it is
# extremely preliminary. It does not work for all datasets.

import yt # yt is Python module like any other
import os
import numpy as np

# Load the Carpet API
from yt.frontends.carpet.api import CarpetDataset

# load in the data
ds = CarpetDataset('second_order_wavetoy/output-0000/ml_wavetoy_so',
                   iteration=0)
ind = ds.index # I don't know why you need this, but you do. It's a bug.

# list available fields:
print ds.field_list

# at time t = 1, load in the data and plot the energy density in the
# xy-plane.
ds = CarpetDataset('second_order_wavetoy/output-0000/ml_wavetoy_so',
                   iteration=1)
slc = yt.SlicePlot(ds,'z','ml_wavetoy-wt_eps')
slc.annotate_grids() # show the refinement boundaries on the plot
# save the plot
slc.save('simple-wavetoy-sliceplot.pdf')

# Do a volume rendering. See isolated_galaxy.py for an in-depth explanation
ds = CarpetDataset('second_order_wavetoy/output-0000/ml_wavetoy_so',
                   iteration=1)
dd = ds.all_data()
use_log = False
field = 'ml_wavetoy-wt_eps'
mi, ma = dd.quantities.extrema(field)
tf = yt.ColorTransferFunction((mi, ma))
c = (ds.domain_right_edge + ds.domain_left_edge)/2.0
L = np.array([1.0, 1.0, 1.0])
W = ds.quan(0.3, 'unitary')
N = 512
cam = ds.camera(c, L, W, N, tf, fields = [field], log_fields = [use_log])
tf.add_layers(10, 0.01, colormap = 'RdBu_r')
im = cam.snapshot('wavetoy_volume_render_it1.png')

