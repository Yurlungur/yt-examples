#!/usr/bin/env python

# shifted_gauge_wave.py
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2015-08-13 22:50:46 (jmiller)>

# This is a yt script showing how to render a slice plot for my
# discontinuous Galerkin data (based on Einstein Toolkit hdf5
# data). This particular example is the shifted gauge wave.

# (Sorry. The code that generates this data is not public yet, so you
# can't run this script. But it will be soon.)
# ============================================================

# yt is a module like any other.
# you just import it.
import yt
import numpy as np
import os
import h5py

# To enable parallelism in yt, just call yt.enable_parallelism() in
# the script and call the script with mpi in the standard way. i.e.,
# mpirun -np 16 python shifted_gauge_wave.py
yt.enable_parallelism()
num_procs = 16 # number of processors to use in parallel

rho=8
num_files = 2

directory="bssn_dg4_trunc_rho_shitfed_gauge_wave_rho_{}/output-0000/shifted_gauge_wave".format(rho)

grid_filename = 'grid-coordinates.xyz.file_{}.h5'
metric_filename = 'admbase-metric.xyz.file_{}.h5'

grid_files = [h5py.File(directory + '/' + grid_filename.format(i))\
              for i in range(num_files)]
metric_files = [h5py.File(directory + '/' + metric_filename.format(i))\
                for i in range(num_files)]

def stitch_z(filename,field_string):
    """
    The parallelization (for this particular simulation) broke the
    grid into constant z slices
    """
    return np.concatenate((filename[0][field_string.format(0)][:-1,0,0],
                           filename[1][field_string.format(1)][1:,0,0]))

# The grid points are the cartesian product
# x_coloc, y_coloc, z_coloc
x_coloc = grid_files[0][u'GRID::x it=0 tl=0 rl=0 c=0'][0,0,...]
y_coloc = grid_files[0][u'GRID::y it=0 tl=0 rl=0 c=0'][0,...,0]
z_coloc = stitch_z(grid_files,u'GRID::z it=0 tl=0 rl=0 c={}')

# yt expects cell-centered data. And the hexahedral mesh data
# structure expects to be told the x,y, and z positions of the
# cells. To feed our data into yt, we interpret our vertexes as cell
# centers and calculate what the edges of those cell centers should
# be.
def get_boundaries_from_centers(quad):
    """
    Given a list of cell centers for a hexahedral mesh in 1D,
    generates cell boundaries.

    Accounts for repeated points.
    """
    out = np.empty((len(quad)-1))
    for i in range(len(quad)-2):
        out[i+1] = 0.5*(quad[i+1]+quad[i+2])
    out[0] = quad[0] - 0.5*(quad[2]-quad[1])
    out[-1] = quad[-1] + 0.5*(quad[-2]-quad[-3])
    return out

# The positions of the cell edges
xgrid,ygrid,zgrid = (get_boundaries_from_centers(grid)\
                     for grid in [x_coloc,y_coloc,z_coloc])

# Arrays that yt uses internally to store the unevenly spaced grid data.
coords,conn = yt.hexahedral_connectivity(xgrid,ygrid,zgrid)

# The size of the domain
bbox = np.array([[np.min(xgrid),np.max(xgrid)],
                 [np.min(ygrid),np.max(ygrid)],
                 [np.min(zgrid),np.max(zgrid)]])

def get_metric_at(iteration):
    """
    At the iteration in the simulation, finds the gxx component of the
    metric as an array.
    """
    field_string = u'ADMBASE::gxx it={}'.format(iteration)+' tl=0 rl=0 c={}'
    return np.concatenate((metric_files[0][field_string.format(0)][:-1],
                           metric_files[1][field_string.format(1)][1:]))

# Get the times in all iterations
relevant_keys = filter(lambda x: 'ADMBASE::gxx' in x, metric_files[0].keys())
timesteps = [None for key in relevant_keys]
time_map = {}
for i,key in enumerate(relevant_keys):
    timesteps[i] = metric_files[0][key].attrs[u'timestep']
    time_map[timesteps[i]] = metric_files[0][key].attrs[u'time']
timesteps.sort()
times = [time_map[t] for t in timesteps]

# Tell yt we want there to be a field 'gxx' that, when plots display
# it, looks right. 'Gxx' will be what we tell yt the field looks like
# natively.
def _gxx(field,data):
    return data['Gxx']
yt.add_field('gxx',function=_gxx,display_name=r'$g_{xx}$')

# Define a function to take a dataset and an iteration, load them in,
# and make a slice plot at constant z = 0. 
def make_sliceplot_at(i):
    iteration = timesteps[i]
    data = {}
    data['Gxx'] = get_metric_at(iteration)
    ds = yt.load_hexahedral_mesh(data,conn,coords,
                                 bbox=bbox,
                                 sim_time = time_map[iteration])
    slc = yt.SlicePlot(ds,'z','gxx')
    slc.set_cmap('gxx', 'RdBu_r')
    ax = slc.plots['gxx'].axes
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set
    slc.save('gauge_wave_animation_frame_{}.png'.format(i))

# and run it to generate the frames for the movie.  We render each
# frame in parallel, since the the process is embarassingly parallel,
# using yt's parallel_objects toolbox.
imax = len(times)#times.index(1.0000000000000002)+1
iterations = [i for i in range(imax)]
for i in yt.parallel_objects(iterations,num_procs):
    make_sliceplot_at(i)

# afterwards, we stitch together the frames into a movie with ffmpeg.

