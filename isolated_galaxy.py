#!/usr/bin/env python

# isolated_galaxy.py
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2015-08-13 22:16:05 (jmiller)>

# This is a yt script showing some simple example volume rendering in
# yt. For simplicity, the volume-rendered animation generates
# individual frames, which can later be stitched together with ffmpeg
# or a similar tool. However, matplotlib.animation can be used to
# stitch the images together in the standard way if you prefer.

# The data for this animation is taken from the IsolatedGalaxy
# simulation, produced in Enzo. The simulation is available here:
# http://yt-project.org/data/

# I used the yt documentation to construct this example:
# http://yt-project.org/doc/
# ==================================================================


# yt is a module like any other.
# you just import it.
import numpy as np
import yt


# To enable parallelism in yt, just call yt.enable_parallelism() in
# the script and call the script with mpi in the standard way. i.e.,
# mpirun -np 48 python isolated_galaxy.py
yt.enable_parallelism()


# First we tell yt to load the data set. If your code format is known,
# you just give yt the appropriate directory and it automagically
# slurps up the relevant information. Things are obviously more
# complicated if your data format is unknown. In that case, you have
# to write a bit of glue code.
# See: http://yt-project.org/doc/examining/loading_data.html
ds = yt.load("IsolatedGalaxy/galaxy0030/galaxy0030")


# To generate a volume plot, we need to tell yt a number of
# details. It needs to know the center of the volume, and the
# dimensions of the volume we want to render. It also needs to know
# the camera angle and where the light source illuminating the volume
# comes from. Finally, yt needs to know the transfer function that
# maps density to color. To tell yt all this, we create a camera
# object that we can manipulate and we feed it the relevant
# information.
# -----------------------------------------------------------------
# the center of the domain
c = (ds.domain_right_edge + ds.domain_left_edge)/2.0

# the field we want to render
field = 'density'

# Do you want the log of the field?
use_log = True

# The max and min of our field
dd = ds.all_data()
mi, ma = dd.quantities.extrema(field)
if use_log:
    mi,ma = np.log10(mi), np.log10(ma)

# Define the transfer function
tf = yt.ColorTransferFunction((mi, ma))
# Tell it we want ten discrete colors each of which is a Gaussian bin
# for the density data of sigma = 0.01
tf.add_layers(10, 0.01, colormap = 'RdBu_r')

# The vector from the center to the light source
L = np.array([1.0, 1.0, 1.0])

# The width of the domain we want to render (in each dimension)
W = ds.quan(0.3,'unitary')

# The number of pixels on a side for the final image
N = 1024

# Create the camera object
# Create a camera object
cam = ds.camera(c, L, W, N, tf,
                fields = [field], log_fields = [use_log])
# -----------------------------------------------------------------


# Now we can make the movie of the static galaxy (at the end of the
# simulation) by moving the camera through the space and taking
# snapshots. We'll zoom in by a factor of 10 over 5 frames and then
# rotate around. At each frame, we'll save an image.
# ------------------------------------------------------------
frame=0
for i, snapshot in enumerate(cam.zoomin(20.0, 40, clip_ratio=8.0)):
    snapshot.write_png('camera_movement_%04i.png' % frame)
    frame += 1
for i, snapshot in enumerate(cam.rotation(2*np.pi,60,clip_ratio=8.0)):
    snapshot.write_png('camera_movement_%04i.png' % frame)
    frame += 1
# ------------------------------------------------------------


# Now, suppose we want to make a phase plot showing temperature,
# density, and the magnitude of velocity. It's as simple as this:
# ------------------------------------------------------------
# recall that dd = ds.all_data()
p = yt.PhasePlot(dd, "density", "temperature",
                 "velocity_magnitude")
# non-volume-rendered plots support vector graphics
p.save('isolated_galaxy_phase_diagram.pdf')
# ------------------------------------------------------------


# Now suppose we want to define the thermal energy density for the gas
# making up the galaxy and plot it in 2d. How can we do that?
# First we define a function that maps the data (which is a dictionary
# mapping string variable names to datasets) to a number. Then we tell
# yt it exists using the function yt.dataset.add_field. Then we can
# just plot it like any other quantity.
# ----------------------------------------------------------------
# First create a function which yields your new derived field
def thermal_energy_dens(field, data):
    return (3/2)*data['gas', 'number_density'] * data['gas', 'kT']
# Then add it to your dataset and define the units
ds.add_field(("gas", "thermal_energy_density"),
             units="erg/cm**3",
             function=thermal_energy_dens)
# A projection plot is what it sounds like. You project the quantity
# onto (in this case) the y-z plane.
p = yt.ProjectionPlot(ds, "x",
                      "thermal_energy_density",
                      weight_field="density",
                      width=(200, 'kpc'))
p.save('isolated_galaxy_thermal_energy_density.pdf')
# ----------------------------------------------------------------

