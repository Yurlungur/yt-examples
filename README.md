# yt Examples

** Author:** Jonah Miller (jonah.maxwell.miller@gmail.com)

**Time-stamp:** 2015-08-13 23:21:05

This is a list of yt scripts that I used to generate the plots in my
talk for the [2015 Einstein Toolkit workshop](http://agenda.albanova.se/conferenceDisplay.py?confId=4936) in NORDITA on visualizing
Einstein Toolkit data with yt. 


I used some [sample
data](http://yt-project.org/doc/examining/loading_data.html) provided
by the yt community. The experimental Carpet frontend for yt can be
found in [Kacper Krowalik's fork of yt](https://bitbucket.org/xarthisius/yt), under the `cactus` bookmark. To
attain it:

`hg clone ssh://hg@bitbucket.org/xarthisius/yt`

`cd yt`

`hg bookmark cactus`

`python setup.py build_ext -i`

if this works, you installed yt. Now just add `$(pwd)` to your `PYTHONPATH`

My slides, and some data, are bundled with these scripts.